// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "j2/Actor components/CooldownComponent.h"
#include "PlayerCharacter.generated.h"

UCLASS()
class J2_API APlayerCharacter : public ACharacter
{
	GENERATED_BODY()

public:

	// Sets default values for this character's properties
	APlayerCharacter();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float HP;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float MAX_HP;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int Ammo;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int MAX_AMMO;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float StepCooldown;

    UPROPERTY(BlueprintReadOnly)
	int KillCounter;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UCooldownComponent* CooldownComponent;
	
protected:

	// The location in which the mouse position line intersect the playable plane.
	// The intersect line goes from the mouse position to the mouse direction multiplied by a given constant
	UPROPERTY(BlueprintReadOnly)
	FVector MousePlaneIntersectionPosition;
	
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Input event for when the player wants to move up or down
	virtual void MoveUp(float InputValue);
     
	// Input event for when the player wants to move right or left
	virtual void MoveRight(float InputValue);

	// Called when the player express the intent to leave the game by pressing 'Esc' on the Keyboard
	virtual void LeaveGame();

	// Called when the player dies (HP <= 0)
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void PlayerDeath();
	virtual void PlayerDeath_Implementation();

	// Spawn a projectile in a given location and shoot it in a given direction.
	// N.B Projectile cost determines how many ammo this projectile it's going to consume
	UFUNCTION(BlueprintCallable, meta = (DeterminesOutputType = "ProjectileType"))
	AActor* Shoot(const TSubclassOf<AActor> ProjectileType, const FTransform& SpawnTransform, const int ProjectileCost);
	
	// Make the player character take a step in a given 2D direction(X, Y) at a given speed
	UFUNCTION(BlueprintCallable)
	void Step(const FVector2D Direction, const float Speed);

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	// Called when the player receive damage
	virtual float TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;
	
	UFUNCTION(BlueprintCallable, BlueprintPure)
	FORCEINLINE bool& IsStepInCooldown() { return bIsStepInCooldown; }

private:
	
	// Called when the step cooldown ends
	UFUNCTION()
	void OnStepCooldownEnd();
	
	// The playable world plane
	FPlane WorldPlane{FVector(0,0,0), FVector(0,0,1)};

	// Step cooldown state
	bool bIsStepInCooldown;
	
	// The handler for cooldown timers
	FTimerHandle CooldownTimer;
};



