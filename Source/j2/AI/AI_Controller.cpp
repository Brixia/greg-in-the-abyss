// Fill out your copyright notice in the Description page of Project Settings.


#include "AI_Controller.h"
#include "Enemy.h"
#include "j2/Libraries/J2_Library.h"
#include "Perception/AIPerceptionComponent.h"
#include "Kismet/GameplayStatics.h"


AAI_Controller::AAI_Controller()
{
	
}

void AAI_Controller::BeginPlay()
{
	Super::BeginPlay();
	Player = Cast<APlayerCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
    // Set player opponent and move towards it.
	SetPlayerOpponent(Player);
	MoveToActor(Player);
}
