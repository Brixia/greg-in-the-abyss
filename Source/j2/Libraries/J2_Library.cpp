// Fill out your copyright notice in the Description page of Project Settings.


#include "J2_Library.h"
#include "j2/J2_GameplayCore/J2_Gamemode.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"

void UJ2_Library::PrintString(const FString DebugMessage, const FColor MessageColor, const float Time)
{
	if(GEngine)
	{
		GEngine->AddOnScreenDebugMessage(INDEX_NONE, Time, MessageColor, DebugMessage, true);
	}
}

FVector2D UJ2_Library::GetRandomXYDirection()
{
	const float X = UKismetMathLibrary::RandomFloatInRange(-1, 1);
	const float Y = UKismetMathLibrary::RandomFloatInRange(-1, 1);
	return FVector2D(X, Y);
}

AActor* UJ2_Library::SpawnActorAtDistanceFromActor(UObject* WorldContext, AActor* TargetActor,
	const TSubclassOf<AActor> ActorClass, const float Distance, const float MinimumDistance,
	const bool SafeSpawn, FVector Direction)
{
	if(Direction.IsZero())
	{
		// if direction is Zero vector, Get random unit direction vector
		const FVector2D XYDirection = GetRandomXYDirection();
		Direction.X = XYDirection.X;
		Direction.Y = XYDirection.Y;
	}

	// Get the target actor location
	const FVector TargetLocation = TargetActor->GetActorLocation();
	// Calculate possible spawn location
	FVector SpawnLocation = Direction * Distance + TargetLocation;
    // Get world from context
	UWorld* World = WorldContext->GetWorld();

	if(SafeSpawn)
	{
		// Check if there is any obstacle from the player to the spawn location, if the result is true then change
		// the spawn location to the impact point with the obstacle
		FHitResult RaycastResult;
		if(World->LineTraceSingleByChannel(RaycastResult, TargetLocation, SpawnLocation, ECC_Visibility))
		{
			SpawnLocation = RaycastResult.ImpactPoint;
		}
	}
	AActor* SpawnedActor = nullptr;
	const float SpawnDistanceFromTarget = UKismetMathLibrary::Vector_Distance(TargetLocation, SpawnLocation);
	if(SpawnDistanceFromTarget >= MinimumDistance)
	{
		SpawnedActor = World->SpawnActor(ActorClass, &SpawnLocation);
	}
	return SpawnedActor;
}

APlayerHUD* UJ2_Library::GetPlayerHUD(UObject* WorldContext)
{
	// Get hud from player controller and cast it to the Player HUD type
	AHUD* PlayerHud = UGameplayStatics::GetPlayerController(WorldContext, 0)->GetHUD();
	return Cast<APlayerHUD>(PlayerHud);
}

AJ2_Gamemode* UJ2_Library::GetJ2Gamemode(UObject* WorldContext)
{
	// Get game mode and cast it to Greg in the Abyss basic game mode type
	return Cast<AJ2_Gamemode>(UGameplayStatics::GetGameMode(WorldContext));
}




