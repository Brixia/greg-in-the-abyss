// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "j2/J2_GameplayCore/J2_Gamemode.h"
#include "j2/Player/PlayerHUD.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "J2_Library.generated.h"

/**
 * 
 */
UCLASS()
class J2_API UJ2_Library : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:

	// wrapper function for GEngine->AddOnScreenDebugMessage method, this is more similar to the Blueprint Print String method
	UFUNCTION(meta = (DevelopmentOnly))
	static void PrintString(const FString DebugMessage, const FColor MessageColor = FColor::Cyan, const float Time = 5.f);
	
	UFUNCTION(BlueprintCallable, BlueprintPure, meta = (Category = "Math"))
	static FVector2D GetRandomXYDirection();

	// Spawn an actor at a given distance from another actor when the condition is true, returns null otherwise.
	//  ContextActor - The Actor from which we will calculate the distance to find the spawn distance
	//  ActorClass - The class of the actor you want to spawn
	//  Distance - The distance at which you want to spawn the new actor
	//  MinimumDistance - The minimum distance from the TargetActor or TargetLocation to the spawn location which will be accepted to
	//                    spawn the new actor. If the distance between spawn location and target location is lower than this value
	//                    actor spawn will fail.
	//  SafeSpawn - When enabled we will trace against other actors to determine if there is something between the ContextActor
	//             and the spawn location, if true the spawn location will be changed to match the trace impact point(usually
	//             we use this to avoid spawning actors outside of the map.
	//  Direction - The direction from the TargetActor. N.B. if 0 we will select a random direction
	UFUNCTION(BlueprintCallable, meta = (Category = "Gameplay", WorldContext = "WorldContext"))
	static AActor* SpawnActorAtDistanceFromActor(UObject* WorldContext, AActor* TargetActor, const TSubclassOf<AActor> ActorClass,
	                                             const float Distance, const float MinimumDistance, const bool SafeSpawn,
	                                             FVector  Direction = FVector(0.f, 0.f, 0.f));

	UFUNCTION(BlueprintCallable, BlueprintPure, meta = (Category = "J2", WorldContext = "WorldContext"))
	static APlayerHUD* GetPlayerHUD(UObject* WorldContext);

	UFUNCTION(BlueprintCallable, BlueprintPure, meta = (Category = "J2", WorldContext = "WorldContext"))
	static AJ2_Gamemode* GetJ2Gamemode(UObject* WorldContext);
};
