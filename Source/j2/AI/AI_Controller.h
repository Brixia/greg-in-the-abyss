// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "j2/Player/PlayerCharacter.h"
#include "Perception/AISenseConfig_Sight.h"
#include "AI_Controller.generated.h"

/**
 * 
 */
UCLASS()
class J2_API AAI_Controller : public AAIController
{
	GENERATED_BODY()

protected:

	virtual void BeginPlay() override;

	// Set the new player opponent for the AI Controlled by this AI Controller
	UFUNCTION(BlueprintCallable)
	void SetPlayerOpponent(APlayerCharacter* NewPlayerOpponent) { Player = NewPlayerOpponent; }
	
public:

	AAI_Controller();
	
	// Return the current opponent for the AI Controlled by this AI controller
	UFUNCTION(BlueprintCallable)
	FORCEINLINE APlayerCharacter* GetPlayerOpponent() const { return Player; }
	
private:

	UPROPERTY()
	UAISenseConfig_Sight* SightConfig;

	// The Player opponent for this AI.
	// This is a single player game, which means that this variable should
	// Always be a player with player index = 0
	UPROPERTY()
	APlayerCharacter* Player;
};

