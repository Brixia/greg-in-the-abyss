// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "j2/AI/Enemy.h"
#include "j2/Player/PlayerCharacter.h"
#include "J2_Gamemode.generated.h"

/**
 * Base game mode class for Greg in the Abyss which contains the basic logic for handling game state events such as
 * victory and lost.
 * This game mode it's the default game mode, meaning that it contains the basic rules for this specific game.
 * If you want to make a new game mode specific to a new level please create a new blueprint class and inherit from this class, not from the
 * standard UE4 game mode
 */
UCLASS()
class J2_API AJ2_Gamemode : public AGameModeBase
{

	GENERATED_BODY()

public:

	AJ2_Gamemode();

	// Get the current player for this game mode
	UFUNCTION(BlueprintGetter, meta = (Category = "Default game mode"))
	FORCEINLINE APlayerCharacter* GetPlayer() const { return Player; }

	// Check if the the victory condition has been met
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	bool CheckVictory();

	// Called when the player has won the game
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void OnVictory();

	// Called when the player has lost the game
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void OnGameOver();
	
protected:

	// Called when the game starts
	virtual void BeginPlay() override;

private:
	
	UPROPERTY(BlueprintGetter="GetPlayer")
	APlayerCharacter* Player;
};
