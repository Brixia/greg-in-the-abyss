// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "j2/Player/PlayerCharacter.h"
#include "AIController.h"
#include "AI_Controller.h"
#include "Enemy.generated.h"

UCLASS()
class J2_API AEnemy : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AEnemy();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float HP;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float MAX_HP;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float MeleeRange;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float MeleeCooldown;

	// The time it takes for this AI character to perform the actual attack.
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float MeleeAttackTime;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Damage;

	// The interval at which the AI Character will request updates about it's movement status
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float MovementUpdateTime;
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called when this actor receive damage
	virtual float TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;

	// Called when the AI character dies
	UFUNCTION(BlueprintNativeEvent)
	void EnemyDeath();
	virtual void EnemyDeath_Implementation();

	// Called when the AI Decides it wants to try a melee attack against the player
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void OnMelee();
	virtual void OnMelee_Implementation();

	// Called when the melee cooldown ends
	UFUNCTION()
	void OnMeleeCooldownEnd();

	// Called when the AI tries to damage player
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void OnAttack();
	void OnAttack_Implementation();
	
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION(BlueprintCallable)
	FORCEINLINE float GetDistanceFromPlayer() const { return DistanceFromPlayer; }
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	UCooldownComponent* CooldownComponent;
	
private:

	// The AI controller which controls this character
	UPROPERTY()
	AAI_Controller* AIController;

	// Cooldown state for Melee attack
	bool bMeleeIsInCooldown;

	// The distance between this AI character and the player
	float DistanceFromPlayer;

	UFUNCTION()
	void OnMovementUpdate();
};




