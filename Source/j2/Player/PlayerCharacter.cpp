// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerCharacter.h"
#include "DrawDebugHelpers.h"
#include "j2/J2_GameplayCore/J2_Gamemode.h"
#include "j2/Libraries/J2_Library.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"

// Sets default values
APlayerCharacter::APlayerCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Initialize default Properties
	HP = 100.f;
	MAX_HP = 100.f;
	Ammo = 25;
	MAX_AMMO = 50;
	bIsStepInCooldown = false;
	StepCooldown = 0.5;
	KillCounter = 0;

	// Create player components
	CooldownComponent = CreateDefaultSubobject<UCooldownComponent>(TEXT("Cooldown Component"));
}

// Called when the game starts or when spawned
void APlayerCharacter::BeginPlay()
{
	Super::BeginPlay();
	UGameplayStatics::GetPlayerController(GetWorld(), 0)->SetShowMouseCursor(true);
}

// Called every frame
void APlayerCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
    const APlayerController* PlayerController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
    // Get mouse X, Y coordinates
	float MouseX;
	float MouseY;
	PlayerController->GetMousePosition(MouseX, MouseY);
	// Get mouse world location and direction
	FVector MouseWorldPosition;
	FVector MouseWorldDirection;
	// Deproject screen position to world, draw a line from mouse position and a given point and then calculate the intersection
	// between the mouse position and the playable plane
	const bool bDeprojectionSuccessful = PlayerController->DeprojectScreenPositionToWorld(MouseX, MouseY,MouseWorldPosition, MouseWorldDirection);
	if(bDeprojectionSuccessful)
	{
		float T;
		UKismetMathLibrary::LinePlaneIntersection(MouseWorldPosition, MouseWorldDirection * 999999.f,
			                                      WorldPlane, T, MousePlaneIntersectionPosition);
		// Find rotation between player location and mouse point and then rotate the player mesh
		FRotator PlayerRotation = UKismetMathLibrary::FindLookAtRotation(GetMesh()->GetComponentLocation(), MousePlaneIntersectionPosition);
		GetMesh()->SetWorldRotation(FRotator(0.f, PlayerRotation.Yaw - 90, 0.f));
	}
}

// Called to bind functionality to input
void APlayerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	// Initialize WASD movement
	PlayerInputComponent->BindAxis("Move Up", this, &APlayerCharacter::MoveUp);
	PlayerInputComponent->BindAxis("Move Right", this, &APlayerCharacter::MoveRight);
	PlayerInputComponent->BindAction("Escape", IE_Pressed, this, &APlayerCharacter::LeaveGame);
}

float APlayerCharacter::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator,
	AActor* DamageCauser)
{
	Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
	HP -= DamageAmount;
	if(HP <= 0)
	{
		PlayerDeath();
	}
	return HP;
}

void APlayerCharacter::Step(const FVector2D Direction, const float Speed)
{
	// Calculate step velocity
	FVector2D Velocity = Direction * Speed;
	// Launch the character in a given direction and decrease ammo value by 1
	LaunchCharacter(FVector(Velocity.X, Velocity.Y, 0.f), true, false);
	Ammo -= 1;
}

void APlayerCharacter::MoveUp(float InputValue)
{
	if(bIsStepInCooldown != true  && InputValue != 0 && Ammo > 0)
	{
		bIsStepInCooldown = true;
		FVector UnrealForwardVector = UKismetMathLibrary::Vector_Forward();
		// Scale UE4 forward vector based on input value
		UnrealForwardVector *= UnrealForwardVector * InputValue;
		Step(FVector2D(UnrealForwardVector.X, UnrealForwardVector.Y), 2500.f);
        // Handle step cooldown
		CooldownComponent->BeginTransientCooldown(StepCooldown, "OnStepCooldownEnd");
	}
}

void APlayerCharacter::MoveRight(float InputValue)
{
	if(bIsStepInCooldown != true && InputValue != 0 && Ammo > 0)
	{
		bIsStepInCooldown = true;
		FVector UnrealRightVector = UKismetMathLibrary::Vector_Right();
		// Scale UE4 forward vector based on input value
		UnrealRightVector *= UnrealRightVector * InputValue;
		Step(FVector2D(UnrealRightVector.X, UnrealRightVector.Y), 2500.f);
		// Handle step cooldown
		CooldownComponent->BeginTransientCooldown(StepCooldown, "OnStepCooldownEnd");
	}
}

void APlayerCharacter::LeaveGame()
{
	// Leave the game
	const UWorld* World = GetWorld();
	UKismetSystemLibrary::QuitGame(World, UGameplayStatics::GetPlayerController(World, 0), EQuitPreference::Quit, false);
}

void APlayerCharacter::PlayerDeath_Implementation()
{
	// Notify game mode that we're entering the game over state
	Cast<AJ2_Gamemode>(UGameplayStatics::GetGameMode(GetWorld()))->OnGameOver();
}

AActor* APlayerCharacter::Shoot(const TSubclassOf<AActor> ProjectileType, const FTransform& SpawnTransform,
	                            const int ProjectileCost)
{
	// Initialize projectile spawn params
	FActorSpawnParameters ActorSpawnParams;
	ActorSpawnParams.Instigator = this;
	ActorSpawnParams.Owner = GetController();
	
	// Spawn projectile
	AActor* Projectile = GetWorld()->SpawnActor(ProjectileType, &SpawnTransform, ActorSpawnParams);

	// For some reason Unreal seems to be ignoring the Scale parameter in SpawnTransform when
	// spawning a new actor, this should enforce the scale change even if it's not optimal(without the if
	// statement might return a nullptr exception, so it is better to check).
	// I might make some research to find a fix/workaround, but for now it works
	if(Projectile)
	{
		Projectile->SetActorScale3D(SpawnTransform.GetScale3D());
	}

	// After projectile spawn, decrease the ammo count by the projectile cost
	Ammo -= ProjectileCost;
	return Projectile;
}

void APlayerCharacter::OnStepCooldownEnd()
{
	bIsStepInCooldown = false;
	//...
}

