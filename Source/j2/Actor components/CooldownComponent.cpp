// Fill out your copyright notice in the Description page of Project Settings.


#include "CooldownComponent.h"
#include "Components/TimelineComponent.h"
#include "j2/Libraries/J2_Library.h"

UCooldownComponent::UCooldownComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}

// Called when the game starts or when spawned
void UCooldownComponent::BeginPlay()
{
	Super::BeginPlay();
}

void UCooldownComponent::TickComponent(float DeltaTime, ELevelTick Tick, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, Tick, ThisTickFunction);
}

void UCooldownComponent::AddCooldown(const FName CooldownAction, const FOnTimelineEvent& CooldownUpdateEvent,
	const FOnTimelineEvent& CooldownEndEvent)
{
	// Create timeline component and initialize timeline component
	UTimelineComponent* CooldownTimeline = NewObject<UTimelineComponent>(this, UTimelineComponent::StaticClass(), CooldownAction);
	CooldownTimeline->RegisterComponent();
	CooldownTimeline->SetTimelineLengthMode(TL_TimelineLength);
	CooldownTimeline->SetTimelinePostUpdateFunc(CooldownUpdateEvent);
	CooldownTimeline->SetTimelineFinishedFunc(CooldownEndEvent);
	// Add cooldown timeline to the actor cooldown map
	ActorCooldowns.Add(CooldownAction, CooldownTimeline);
}

void UCooldownComponent::AddCooldownWithCurve(const FName CooldownAction, UCurveFloat* CooldownCurve,
	const FOnTimelineFloat& CooldownUpdateEvent, const FOnTimelineEvent& CooldownEndEvent)
{
	// Create timeline component and initialize timeline component
	UTimelineComponent* CooldownTimeline = NewObject<UTimelineComponent>(this, UTimelineComponent::StaticClass(), CooldownAction);
	CooldownTimeline->RegisterComponent();
	CooldownTimeline->SetTimelineLengthMode(TL_LastKeyFrame);
	CooldownTimeline->AddInterpFloat(CooldownCurve, CooldownUpdateEvent, CooldownAction);
	CooldownTimeline->SetTimelineFinishedFunc(CooldownEndEvent);
	// Add cooldown timeline to the actor cooldown map
	ActorCooldowns.Add(CooldownAction, CooldownTimeline);
}

void UCooldownComponent::BeginTransientCooldown(const float Cooldown,  const FName CallbackEventName)
{
	FTimerHandle CooldownHandle;
	FTimerDelegate CooldownEndCallback;
	CooldownEndCallback.BindUFunction(GetOwner(), CallbackEventName);
	GetWorld()->GetTimerManager().SetTimer(CooldownHandle, CooldownEndCallback, Cooldown, false);
}

void UCooldownComponent::BeginCooldown(const FName CooldownAction, const float Cooldown, const bool OverrideCooldown)
{
	UTimelineComponent* CooldownTimeline = GetTimelineByCooldownName(CooldownAction);
	if(CooldownTimeline)
	{
		if(OverrideCooldown || !CooldownTimeline->IsPlaying())
		{
			CooldownTimeline->SetTimelineLength(Cooldown);
			CooldownTimeline->PlayFromStart();
		}
	}
}

void UCooldownComponent::PauseCooldown(const FName CooldownAction)
{
	UTimelineComponent* CooldownTimeline = GetTimelineByCooldownName(CooldownAction);
	if(CooldownTimeline)
	{
		GetTimelineByCooldownName(CooldownAction)->Stop();
	}
}

void UCooldownComponent::ResumeCooldown(const FName CooldownAction)
{
	UTimelineComponent* CooldownTimeline = GetTimelineByCooldownName(CooldownAction);
	if(CooldownTimeline)
	{
		CooldownTimeline->Play();
	}
}

void UCooldownComponent::InvalidateCooldown(const FName CooldownAction)
{
	UTimelineComponent* CooldownTimeline = GetTimelineByCooldownName(CooldownAction);
	if(CooldownTimeline)
	{
		ActorCooldowns.Remove(CooldownAction);
		CooldownTimeline->DestroyComponent();
	}
}

TArray<FName> UCooldownComponent::GetCurrentlyRunningCooldownActions() const
{
	// List of all component registered cooldown actions
	TArray<FName> Actions;
	// List of all currently running cooldown actions
	TArray<FName> RunningCooldownActions;
	ActorCooldowns.GetKeys(Actions);
	for(const FName Action : Actions)
	{
		const UTimelineComponent* CooldownTimeline = *ActorCooldowns.Find(Action);
		if(CooldownTimeline->IsPlaying())
		{
			RunningCooldownActions.Add(Action);
		}
	}
	return RunningCooldownActions;
}


