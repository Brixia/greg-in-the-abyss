// Fill out your copyright notice in the Description page of Project Settings.

#include "PlayerHUD.h"
#include "Kismet/GameplayStatics.h"

APlayerHUD::APlayerHUD()
{
}

void APlayerHUD::BeginPlay()
{
	Super::BeginPlay();

	// Initialize and display default player widgets
	for(const FPlayerUIParams& WidgetParams : DefaultPlayerUI)
	{
		if(WidgetParams.DisplayOnPlayerScreen)
		{
			CreateAndAddWidgetToPlayerScreen(WidgetParams.Widget, WidgetParams.ZOrder);
		} else
		{
			CreateAndAddWidgetToViewport(WidgetParams.Widget, WidgetParams.ZOrder);
		}
	}
}

void APlayerHUD::CreateAndAddWidgetToPlayerScreen(const TSubclassOf<UUserWidget> WidgetClass, const int ZOrder)
{
	UUserWidget* CreatedWidget = CreateWidget<UUserWidget, APlayerController>(UGameplayStatics::GetPlayerController(GetWorld(), 0), WidgetClass);
	CreatedWidget->AddToPlayerScreen(ZOrder);
	PlayerWidgets.Add(WidgetClass, CreatedWidget);
}

void APlayerHUD::CreateAndAddWidgetToViewport(const TSubclassOf<UUserWidget> WidgetClass, const int ZOrder)
{
	UUserWidget* CreatedWidget = CreateWidget<UUserWidget, APlayerController>(UGameplayStatics::GetPlayerController(GetWorld(), 0), WidgetClass);
	CreatedWidget->AddToViewport(ZOrder);
	PlayerWidgets.Add(WidgetClass, CreatedWidget);
}

void APlayerHUD::RemoveWidget(UUserWidget* Widget)
{
	Widget->RemoveFromParent();
	PlayerWidgets.Remove(Widget->StaticClass());
}

