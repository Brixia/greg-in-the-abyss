// Fill out your copyright notice in the Description page of Project Settings.


#include "Enemy.h"

#include "GameFramework/CharacterMovementComponent.h"
#include "j2/Libraries/J2_Library.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
AEnemy::AEnemy()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Initialize AI Default properties
    HP = 100.f;
	MAX_HP = 100.f;
	MeleeRange = 100.f;
	MeleeCooldown = 2.f;
	MeleeAttackTime = 1.f;
	MovementUpdateTime = 0.1;
	bMeleeIsInCooldown = false;
	AIControllerClass = AAI_Controller::StaticClass();
   
    // Create AI Components
	CooldownComponent = CreateDefaultSubobject<UCooldownComponent>(TEXT("Cooldown Component"));
	
}

// Called when the game starts or when spawned
void AEnemy::BeginPlay()
{
	Super::BeginPlay();
	// Initialize AI Controller ptr and player character ptr
	AIController = Cast<AAI_Controller>(GetController());

	// Initialize movement update routine
	FTimerHandle MovementUpdateTimerHandle;
	FTimerDelegate MovementUpdateEvent;
	MovementUpdateEvent.BindUFunction(this, "OnMovementUpdate");
	GetWorld()->GetTimerManager().SetTimer(MovementUpdateTimerHandle, MovementUpdateEvent, MovementUpdateTime, true);
}

void AEnemy::OnMovementUpdate()
{
	// Get distance between the AI and the player, if we are in Melee range start executing the melee attack,
	// otherwise keep moving towards the player
	DistanceFromPlayer = GetDistanceTo(AIController->GetPlayerOpponent());
	if (DistanceFromPlayer <= MeleeRange && bMeleeIsInCooldown == false)
	{
		OnMelee();
	} else
	{
		if(GetCharacterMovement()->Velocity.IsZero() && DistanceFromPlayer > MeleeRange)
		{
			AIController->MoveToActor(AIController->GetPlayerOpponent());
		}
	}
}

float AEnemy::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator,
	AActor* DamageCauser)
{
	Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
	HP -= DamageAmount;
	if(HP <= 0)
	{
		EnemyDeath();
	}
	return HP;
}

void AEnemy::EnemyDeath_Implementation()
{
	AIController->GetPlayerOpponent()->KillCounter += 1;
	Destroy();
}

void AEnemy::OnMelee_Implementation()
{
	bMeleeIsInCooldown = true;
	CooldownComponent->BeginTransientCooldown(MeleeCooldown, "OnMeleeCooldownEnd");
}

void AEnemy::OnMeleeCooldownEnd()
{
	bMeleeIsInCooldown = false;
    // Make a delay of a given amount of time to allow the player to escape the attack
	FTimerHandle DelayTimer;
	FTimerDelegate TimerDelegate;
	TimerDelegate.BindUFunction(this,  "OnAttack");
	GetWorld()->GetTimerManager().SetTimer(DelayTimer, TimerDelegate, MeleeAttackTime, false);
	//...
}

void AEnemy::OnAttack_Implementation()
{
	if(DistanceFromPlayer <= MeleeRange)
	{
		UGameplayStatics::ApplyDamage(AIController->GetPlayerOpponent(), 10.f, AIController,
									  this, UDamageType::StaticClass());
	}
}

// Called every frame
void AEnemy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void AEnemy::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

