// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "GameFramework/HUD.h"
#include "PlayerHUD.generated.h"

USTRUCT(BlueprintType)
struct FPlayerUIParams
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere)
	TSubclassOf<UUserWidget> Widget;

	UPROPERTY(EditAnywhere)
	bool DisplayOnPlayerScreen;
	
	UPROPERTY(EditAnywhere)
	int ZOrder;
};
/**
 * 
 */
UCLASS()
class J2_API APlayerHUD : public AHUD
{
	GENERATED_BODY()

public:

	// Default constructor
	APlayerHUD();
	
	// Wrapper method which creates a new widget and automatically adds it to the player screen
	UFUNCTION(BlueprintCallable, meta = (Category = "Player HUD"))
	void CreateAndAddWidgetToPlayerScreen(const TSubclassOf<UUserWidget> WidgetClass, const int ZOrder);

	// Wrapper method which creates a new widget and automatically adds it to the viewport
	UFUNCTION(BlueprintCallable, meta = (Category = "Player HUD"))
	void CreateAndAddWidgetToViewport(const TSubclassOf<UUserWidget> WidgetClass, const int ZOrder);

	// Get currently displayed player widget by class.
	// if the widget is present returns it, otherwise returns null
	UFUNCTION(BlueprintCallable, meta = (DeterminesOutputType = "WidgetClass", Category = "Player HUD"))
	FORCEINLINE UUserWidget* GetPlayerWidgetByClass(const TSubclassOf<UUserWidget> WidgetClass) const { return PlayerWidgets.FindRef(WidgetClass); }

	// Get currently displayed viewport widgets
	UFUNCTION(BlueprintCallable, meta = (Category = "Player HUD"))
	FORCEINLINE TArray<UUserWidget*> GetViewportWidgets() const
	{
		TArray<UUserWidget*> ViewportWidgets;
		for (const TPair<TSubclassOf<UUserWidget>, UUserWidget*>& Pair : PlayerWidgets)
		{
			if(Pair.Value->IsInViewport())
			{
				ViewportWidgets.Add(Pair.Value);
			}
		}
		return ViewportWidgets;
	}

	// Get currently displayed PlayerScreen widgets
	UFUNCTION(BlueprintCallable, meta = (Category = "Player HUD"))
	FORCEINLINE TArray<UUserWidget*> GetPlayerScreenWidgets() const
	{
		TArray<UUserWidget*> PlayerScreenWidgets;
		for(const TPair<TSubclassOf<UUserWidget>, UUserWidget*>& Pair : PlayerWidgets)
		{
			if(!Pair.Value->IsInViewport())
			{
				PlayerScreenWidgets.Add(Pair.Value);
			}
		}
		return PlayerScreenWidgets;
	}
	
	// Remove a widget from the player widgets list and from the player screen/viewport
	UFUNCTION(BlueprintCallable, meta = (Category = "Player HUD"))
	void RemoveWidget(UUserWidget* Widget);

protected:

	// Called when the game starts
	virtual void BeginPlay() override;
	
private:

	// Map of currently displayed player widgets. N.B. for each type of widget we can display only one copy at a time
	UPROPERTY()
	TMap<TSubclassOf<UUserWidget>, UUserWidget*> PlayerWidgets;

	// The default widgets to display when the game starts
	UPROPERTY(EditAnywhere)
	TArray<FPlayerUIParams> DefaultPlayerUI;
};
