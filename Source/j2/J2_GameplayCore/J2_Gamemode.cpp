// Fill out your copyright notice in the Description page of Project Settings.

#include "J2_Gamemode.h"
#include "j2/Libraries/J2_Library.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"

AJ2_Gamemode::AJ2_Gamemode()
{
}

void AJ2_Gamemode::BeginPlay()
{
	Super::BeginPlay();
	// Get player character
	Player = Cast<APlayerCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
}



