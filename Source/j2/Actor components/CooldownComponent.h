// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/TimelineComponent.h"
#include "CooldownComponent.generated.h"

class UTimelineComponent;

// Wrapper component for handling cooldown events.
UCLASS()
class J2_API UCooldownComponent final : public UActorComponent
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	UCooldownComponent();
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	// Add a new cooldown action for this component with both a Update and end callback
	UFUNCTION(BlueprintCallable, Category="Gameplay")
	void AddCooldown(const FName CooldownAction, const FOnTimelineEvent& CooldownUpdateEvent, const FOnTimelineEvent& CooldownEndEvent);

	// Add a new cooldown action for this component with both a Update and end callback which will be based on
	// a cooldown curve
	UFUNCTION(BlueprintCallable, Category="Gameplay")
	void AddCooldownWithCurve(const FName CooldownAction, UCurveFloat* CooldownCurve, const FOnTimelineFloat& CooldownUpdateEvent,
		                      const FOnTimelineEvent& CooldownEndEvent);

	// Begin a new transient cooldown from the start.
	// A transient cooldown it's a cooldown which is not bound to any cooldown action and therefore it
	// would be cleared and deleted when the cooldown ends(it works like a non looping timer)
	UFUNCTION(BlueprintCallable, Category="Gameplay")
	void BeginTransientCooldown(const float Cooldown, const FName CallbackEventName);
	
	// Begin a new cooldown from the start.
	// - if OverrideCooldown is set to true the new cooldown will override the currently running cooldown
	UFUNCTION(BlueprintCallable, Category="Gameplay")
	void BeginCooldown(const FName CooldownAction, const float Cooldown, const bool OverrideCooldown);
	
	UFUNCTION(BlueprintCallable, Category="Gameplay")
	void PauseCooldown(const FName CooldownAction);
	
	UFUNCTION(BlueprintCallable, Category="Gameplay")
	void ResumeCooldown(const FName CooldownAction);

	// Stop a cooldown and remove it from the Cooldown actions list
	UFUNCTION(BlueprintCallable, Category="Gameplay")
	void InvalidateCooldown(const FName CooldownAction);
	
	// Get how many time has passed since the cooldown begun
	UFUNCTION(BlueprintCallable, Category="Gameplay")
	FORCEINLINE float GetCooldownProgress(const FName CooldownAction) const
	{
		const UTimelineComponent* Timeline = *ActorCooldowns.Find(CooldownAction);
		return Timeline->GetPlaybackPosition();
	}

	// Get the timeline component which is responsible of handling the given cooldown action
	UFUNCTION(BlueprintCallable, Category="Gameplay")
	FORCEINLINE UTimelineComponent* GetTimelineByCooldownName(const FName CooldownAction) const { return *ActorCooldowns.Find(CooldownAction); }
	
	// Get all registered cooldown names
	UFUNCTION(BlueprintCallable, Category="Gameplay")
	FORCEINLINE TArray<FName> GetAllCooldownNames() const
	{
		TArray<FName> Keys;
		ActorCooldowns.GetKeys(Keys);
		return Keys;
	}
    // Get all currently running cooldown actions
	UFUNCTION(BlueprintCallable, Category="Gameplay")
	FORCEINLINE TArray<FName> GetCurrentlyRunningCooldownActions() const;
    
private:

	UPROPERTY()
	TMap<FName, UTimelineComponent*> ActorCooldowns;
};

